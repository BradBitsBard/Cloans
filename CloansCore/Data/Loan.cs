﻿using System;

namespace CloansCore
{
    public class Loan
    {
        public string ID { get; set; }
        public DateTime InitializedDate { get; set; }
        public DateTime DueDate { get; set; }
        public decimal AmountOwed { get; set; }
    }
}