﻿using System;
using System.Collections.Generic;

namespace CloansCore
{
    public class Budget
    {
        public List<Tuple<string, decimal>> Incomes;
        public List<Tuple<string, decimal>> Expenses;
    }
}