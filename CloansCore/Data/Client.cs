﻿namespace CloansCore
{
    public class Client
    {
        public string GovernmentIssuedID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public Address Address { get; set; }
        public Budget Budget { get; set; }
    }
}