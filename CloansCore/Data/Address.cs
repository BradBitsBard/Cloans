﻿namespace CloansCore
{
    public struct Address
    {
        public string Street { get; set; }
        public string Town { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
    }
}